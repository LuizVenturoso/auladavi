<?php
/*
    require_once 'classes/Conexao.php';
    
    $conexa = new Conexao();
    echo $conexa->Status();
 * 
 */
?>


<html>
    <head>
        <meta charset="UTF-8">
        <title>Redes</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
    </head>
    <body>

        <table border="0" width="100%"  class="mt-4" id="principal">
            <thead>
                <tr >
                    <td align="center" class="p-1 mb-1 bg-secondary text-white font-weight-bold"> Cadastro de Pessoa </td>
                </tr>
            </thead>
            <tbody>
                <tr >
                    <td> 
                        <form name="for-cadastro" method="POST" enctype="multipart/form-data" action="gravaFormulario.php"  id="frm-cad">
                            <input type="hidden" name="acao" value="cadastrar">
                            <table border="1" width="100%" class="mt-2">
                                <tbody>
                                    <tr>
                                        <td>
                                            <input type="text" name="nome"  class="form-control" placeholder="Nome da Pessoa">
                                        </td>
                                        <td>
                                            <input type="text" name="email"  class="form-control" placeholder="Informe seu email">
                                        </td>
                                        <td>
                                            <input type="text" name="cpf"  class="form-control" placeholder="CPF">
                                        </td>
                                        <td>
                                            <select name="tipoContato"  class="form-control">
                                               <option value="S">Selecione</option> 
                                                <?php
                                                //Busca Tipo De Contatos
                                                ?>
                                                
                                            </select>
                                                <input type="text" name="Contato"  class="form-control" placeholder="Contato">
                                       
                                        </td>
                                        
                                        <td width="250px">
                                            <div class="form-check ml-2">
                                                <input class="form-check-input" type="checkbox" name="ativo" id="ativo">
                                                <label class="form-check-label" for="ativo">
                                                    Ativo
                                                </label>
                                            </div> 
                                        </td>
                                        <td> 
                                            <button name="botao" value="cadastrar" type="submit" class="btn btn-primary" id="cadastrar">Cadastrar</button>
                                           <span id="carregamento"></span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </td>
                </tr>
                <tr height="90px">
                    <td>      
                        <div id="registros">
                                 <?php
            include "./buscaRegistros.php";
            ?>
                            
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
       
   

    </body>
</html>
